import glob 
import errno 
import os
import pandas as pd

#setting paths
Inputpath = './csv_files/*.csv' 
pandas_merged_file = './output/pandas_merged_file.csv'

Inputfiles = glob.glob(Inputpath)

#creating dataframe
df = pd.DataFrame (columns = ['Dependency','project(s)'])
for filename in Inputfiles:
    try:
        with open(filename,'r') as f:
            for lines in f.readlines()[0].split(","):
                    #append data from all file into dataframe
                    row = pd.Series([lines,filename.split("\\")[-1].split(".")[0]],index=['Dependency','project(s)'])
                    df = df.append(row,ignore_index=True)
    except IOError as exc:
            if exc.errno != errno.EISDIR: 
                raise
# Dependency wise Grouping data and export it as csv
df.groupby(['Dependency'])['project(s)'].apply(','.join).reset_index().to_csv(pandas_merged_file,index=False)