import glob 
import errno 
import os
 
#setting paths
Inputpath = './Text_files/*.txt' 
merged_file = './output/simple_merged_file.txt'

Inputfiles = glob.glob(Inputpath)

with open(merged_file,'w+') as m:
    for name in Inputfiles: 
        try:
            with open(name,'r') as f: 
                for i in f.readlines():

                    library = i.strip("\n") #removing '\n' from end of each line
                    project = name.split("\\")[-1] #taking only filename from whole path

                    #setting merged file pointer to zero to read file from beginning.
                    m.seek(0,0)
                    flag = 0

                    #when merged file is not empty
                    if os.stat(merged_file).st_size!=0:
                        m_lines = m.readlines()
                        #checking whether, new python 'library' is already available in merged file or not
                        for i, line in enumerate(m_lines):
                            if library in line.split(":")[0]:
                                #update new projects on lines of mathched python 'library'
                                m_lines[i] = m_lines[i].strip() + ", "+ project +"\n"
                                flag = 1

                        #setting merged file pointe to zero to write updated lines       
                        m.seek(0,0)
                        for line in m_lines:
                            m.write(line)

                        #When no match occure in merged file, then write dependencies with project name in new line
                        if flag==0:
                            m.write(library+" : "+ project+"\n")
                    
                    #First time writing when merged file is empty        
                    else:
                        m.write(library+" : "+ project+"\n")
        except IOError as exc:  
            if exc.errno != errno.EISDIR: 
                raise 